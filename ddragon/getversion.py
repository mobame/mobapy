#!/usr/bin/env python3.6

import requests
from datetime import datetime
import MySQLdb as mariadb

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# NEVER USE THIS DIRECTLY IN PRODUCTION. WILL HAVE TEMPORAL PERFORMANCE INPACT.
# WILL ONLY NEED TO ACTIVATE IF WE'RE NOT USING THE LATEST VERSION OF DDRAGON.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Before anything we initiate the connection to the database. This won't be the last time no thanks to the multiprocessing.
connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')
cursor = connection.cursor()

global current

def webhook(wh_data):
    wh_url = 'https://discordapp.com/api/webhooks/466689045897150474/uksb0ILf5Fq8XSycaHtneZP5Psz0_k5YUtLxHq5xPMAFLoHGNwtPl0F1I6WjwE64cIV0'
    wh_headers = {"User-Agent": "100 Weebs (https://www.mobame.com, v0.1)", "Content-Type": "application/json"}

    r = requests.post(wh_url, headers=wh_headers, json=wh_data)

# I don't know the benefit for this.
headers = {'user-agent': 'mobame (https://mobame.com)'}

# We only need the NA realm.
cfg_versions = 'https://ddragon.leagueoflegends.com/realms/na.json'
versions = requests.get(cfg_versions, headers=headers).json()

cursor.execute("""SELECT version FROM mm_version WHERE realm=%s""", ('na',))

for version in cursor:
    current = version[0]

# Check if we're a version behind before initiating updates.
if current < versions['v']:
    print("Time to update!")
    webhook({"content": "Time to update!"})

    # Update the versions database with the realm's data.
    cursor.execute("""UPDATE mm_version SET version=%s, updated=%s""", (versions['v'], datetime.now().strftime('%Y-%m-%d %H:%M:%S'),))

    print("You're now on the latest version, {}!".format(versions['v']))
    webhook({"content": "You're now on the latest version, {}".format(versions['v'])})

    # Push the update.
    connection.commit()
else:
    # Nothing to worry about, let's go.
    print("Status: You're still on the latest version.")
    webhook({"content": "Status: You're still on the latest version."})

# Done. Move on.
connection.close()
exit(0)