#!/usr/bin/env python3.6

import requests
import json
import MySQLdb as mariadb
import multiprocessing
import subprocess

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# NEVER USE THIS DIRECTLY IN PRODUCTION. WILL HAVE TEMPORAL PERFORMANCE INPACT.
# WILL ONLY NEED TO ACTIVATE IF WE'RE NOT USING THE LATEST VERSION OF DDRAGON.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Before anything we initiate the connection to the database. This won't be the last time no thanks to the multiprocessing.
connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')
cursor = connection.cursor()

def webhook(wh_data):
    wh_url = 'https://discordapp.com/api/webhooks/466689045897150474/uksb0ILf5Fq8XSycaHtneZP5Psz0_k5YUtLxHq5xPMAFLoHGNwtPl0F1I6WjwE64cIV0'
    wh_headers = {"User-Agent": "100 Weebs (https://www.mobame.com, v0.1)", "Content-Type": "application/json"}

    r = requests.post(wh_url, headers=wh_headers, json=wh_data)

# I don't know the benefit for this.
headers = {'user-agent': 'mobame (https://mobame.com)'}

# We only need the NA realm.
cfg_versions = 'https://ddragon.leagueoflegends.com/realms/na.json'
versions = requests.get(cfg_versions, headers=headers).json()

# We're grabbing all available languaes. Nutty.
cfg_languages = 'https://ddragon.leagueoflegends.com/cdn/languages.json'
languages = requests.get(cfg_languages, headers=headers).json()

ver = versions['n']['item']

# Truncate the item table.
cursor.execute("TRUNCATE TABLE mm_item")
print("Truncated item database.")

# Done. Let's move on.
connection.commit()
connection.close()

# This will be used in multiprocessing. So each available language will be loaded quicker. Cause bro...each file is like less than 4MB.
def get_items(language):
    # Initiate the seperate connection since this is virtually going into it's own python script for multiprocessing.
    item_connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    item_connection.set_character_set('utf8')
    item_cursor = item_connection.cursor()

	# Get the latest version available for items.
    data_path = 'https://ddragon.leagueoflegends.com/cdn/%s/data/%s/item.json' % (ver, language)
    datasets = requests.get(data_path, headers=headers).json()
    print('Loaded ' + language)
    for key, val in datasets["data"].items():
        # Gotta make sure some of these even exist

        # coloq
        try:
            colloq = val["colloq"]
        except KeyError:
            colloq = None

        # comsumed
        try:
            comsumed = val["comsumed"]
        except KeyError:
            comsumed = 0

        # plaintext
        try:
            plaintext = val["plaintext"]
        except KeyError:
            plaintext = None

        # stacks
        try:
            stacks = val["stacks"]
        except KeyError:
            stacks = None

        # in_store
        try:
            in_store = val["inStore"]
        except KeyError:
            in_store = None

        # item_from
        try:
            item_from = val["from"]
        except KeyError:
            item_from = None

        # into
        try:
            item_into = val["into"]
        except KeyError:
            item_into = None

        # hide_from_all
        try:
            hide_from_all = val["hideFromAll"]
        except KeyError:
            hide_from_all = None

        # effect
        try:
            effect = val["effect"]
        except KeyError:
            effect = None

        # depth
        try:
            depth = val["depth"]
        except KeyError:
            depth = None

        item_cursor.execute(
            """INSERT INTO mm_item (lang, id, name, description, colloq, consumed, plaintext, stacks, in_store, item_from, item_into, hide_from_all, image, gold, tags, maps, stats, effect, depth) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
            (
                language, #varchar
                key, #bigint
                val["name"], #varchar
                val["description"], #longtext
                colloq, #varchar
                comsumed, #tinyint
                plaintext, #longtext
                stacks, #int
                in_store, #tinyint
                json.dumps(item_from, ensure_ascii=False), #json
                json.dumps(item_into, ensure_ascii=False), #json
                hide_from_all, #tinyint
                json.dumps(val["image"], ensure_ascii=False), #json
                json.dumps(val["gold"], ensure_ascii=False), #json
                json.dumps(val["tags"], ensure_ascii=False), #json
                json.dumps(val["maps"], ensure_ascii=False), #json
                json.dumps(val["stats"], ensure_ascii=False), #json
                effect, #json
                depth #bigint
            )
        )

    # Finally. We've added all the item & locales.
    item_connection.commit()
    item_connection.close()

# JFC this is ballsy. Hope DigitalOcean doesn't get mad for this.
pool = multiprocessing.Pool(56)
pool.imap_unordered(get_items, languages)
pool.close()
pool.join()

print("Erasing all memory cache.")
subprocess.Popen("redis-cli -s /var/run/redis/redis-server.sock flushall", shell=True)

webhook({"content": "Item data now updated on all available locales."})

# ESKETIT
exit(0)