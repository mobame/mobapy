#!/usr/bin/env python3.6

import requests
import json
import MySQLdb as mariadb
import multiprocessing
import subprocess

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# NEVER USE THIS DIRECTLY IN PRODUCTION. WILL HAVE TEMPORAL PERFORMANCE INPACT.
# WILL ONLY NEED TO ACTIVATE IF WE'RE NOT USING THE LATEST VERSION OF DDRAGON.
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Before anything we initiate the connection to the database. This won't be the last time no thanks to the multiprocessing.
connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')
cursor = connection.cursor()

def webhook(wh_data):
    wh_url = 'https://discordapp.com/api/webhooks/466689045897150474/uksb0ILf5Fq8XSycaHtneZP5Psz0_k5YUtLxHq5xPMAFLoHGNwtPl0F1I6WjwE64cIV0'
    wh_headers = {"User-Agent": "100 Weebs (https://www.mobame.com, v0.1)", "Content-Type": "application/json"}

    r = requests.post(wh_url, headers=wh_headers, json=wh_data)

# I don't know the benefit for this.
headers = {'user-agent': 'mobame (https://mobame.com)'}

# We only need the NA realm.
cfg_versions = 'https://ddragon.leagueoflegends.com/realms/na.json'
versions = requests.get(cfg_versions, headers=headers).json()

# We're grabbing all available languaes. Nutty.
cfg_languages = 'https://ddragon.leagueoflegends.com/cdn/languages.json'
languages = requests.get(cfg_languages, headers=headers).json()

ver = versions['n']['champion']

# Truncate the champion table.
cursor.execute("TRUNCATE TABLE mm_champion")
print("Truncated champion database.")

# Done. Let's move on.
connection.commit()
connection.close()

# This will be used in multiprocessing. So each available language will be loaded quicker. Cause bro...each file is like less than 4MB.
def get_champions(language):
    # Initiate the seperate connection since this is virtually going into it's own python script for multiprocessing.
    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    connection.set_character_set('utf8')
    cursor = connection.cursor()

	# Get the latest version available for champions.
    data_path = 'https://ddragon.leagueoflegends.com/cdn/%s/data/%s/championFull.json' % (ver, language)
    datasets = requests.get(data_path, headers=headers).json()
    print('Loaded ' + language)
    for key, val in datasets["data"].items():
        cursor.execute(
            """INSERT INTO mm_champion (lang, id, name, name_canonical, title, image, skins, lore, blurb, allytips, enemytips, tags, partype, info, stats, spells, passive, recommended) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
            (
                language, #varchar
                int(val["key"]), #bigint
                val["name"], #varchar
                val["id"].lower(), #varchar
                val["title"], #varchar
                json.dumps(val["image"], ensure_ascii=False), #json
                json.dumps(val["skins"], ensure_ascii=False), #json
                val["lore"], #longtext
                val["blurb"], #longtext
                json.dumps(val["allytips"], ensure_ascii=False), #json
                json.dumps(val["enemytips"], ensure_ascii=False), #json
                json.dumps(val["tags"], ensure_ascii=False), #json
                val["partype"], #varchar
                json.dumps(val["info"], ensure_ascii=False), #json
                json.dumps(val["stats"], ensure_ascii=False), #json
                json.dumps(val["spells"], ensure_ascii=False), #json
                json.dumps(val["passive"], ensure_ascii=False), #json
                json.dumps(val["recommended"], ensure_ascii=False) #json
            )
        )

    # Finally. We've added all the champions & locales.
    connection.commit()
    connection.close()

# JFC this is ballsy. Hope DigitalOcean doesn't get mad for this.
pool = multiprocessing.Pool(56)
pool.imap_unordered(get_champions, languages)
pool.close()
pool.join()

print("Erasing all memory cache.")
subprocess.Popen("redis-cli -s /var/run/redis/redis-server.sock flushall", shell=True)

webhook({"content": "Champion data now updated on all available locales."})

# ESKETIT
exit(0)