#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import time
import argparse
import requests
import json
import multiprocessing
import MySQLdb as mariadb
from pusher import Pusher
from postwebhooks import webhook
from urllib.parse import unquote

parser = argparse.ArgumentParser()
# General
parser.add_argument("--discorduser", type=int)
# Riot
parser.add_argument("--platform", type=str)
parser.add_argument("--id", type=str)
parser.add_argument("--accountid", type=int)
parser.add_argument("--key", type=str)
# Pusher
parser.add_argument("--pusherid", type=str)
parser.add_argument("--pusherkey", type=str)
parser.add_argument("--pushersecret", type=str)
# SQL
parser.add_argument("--dbuser", type=str)
parser.add_argument("--dbpass", type=str)
parser.add_argument("--database", type=str)
parser.add_argument("--dbsocket", type=str)
args, uknown = parser.parse_known_args()

champion = {}

pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)

pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Updating champion masteries.'})

connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')

cursor = connection.cursor()
cursor.execute("SELECT id, name, name_canonical FROM mm_champion WHERE lang=%s", ('en_US',))

for id, name, name_canonical in cursor:
    champion[id] = name

# First Task: Get summoner's champion mastery data (summoner id)
def get_mastery():
    res = requests.get(url="https://{}.api.riotgames.com/lol/champion-mastery/v3/champion-masteries/by-summoner/{}".format(args.platform, args.id), headers = {"X-Riot-Token": args.key})

    if res.status_code == 200: return res.json()
    if res.status_code == 400: data = {"content": "<@{}>! Bad request in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 404: data = {"content": "<@{}>! Summoner {} was not found in {}. But how did we even get to this step?".format(args.discorduser, args.id, args.platform.upper())}
    if res.status_code == 405: data = {"content": "<@{}>! Method not found in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {} while searching for summoner {}'s mastery".format(args.discorduser, args.platform.upper(), args.id)}
    if res.status_code != 200: webhook(data)

masteries = get_mastery()
sortedmasteries = sorted(masteries, key=lambda x : x['championPoints'], reverse=True)

#print(json.dumps(sortedmasteries, ensure_ascii=False))

# Grab the champion with the highest mastery points.
mainId = sortedmasteries[0]['championId']

# Search for the champion, needle in a haystack two-fold
main_data = champion[mainId]

pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Champion masteries updated.', 'main': main_data.lower(), 'progress': 0.25})

# Updating champion masteries for summoner. But while we're at it...
# Making sure the summoner's "new" status is turned off to prevent repeat updates. Might as well start somewhere.
cursor.execute ("""UPDATE mm_summoner SET champion_masteries=%s, is_new=%s, updated=%s WHERE id=%s AND platform_id=%s""", (json.dumps(sortedmasteries, ensure_ascii=False), False, time.strftime('%Y-%m-%d %H:%M:%S'), args.id, args.platform))

connection.commit()
connection.close()

exit(0)