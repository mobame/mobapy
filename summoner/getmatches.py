#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import argparse
import requests
import json
import multiprocessing
import MySQLdb as mariadb
from pusher import Pusher
from dominate.tags import *
from postwebhooks import webhook
from generatematch import generate_match
from urllib.parse import unquote

parser = argparse.ArgumentParser()
# General
parser.add_argument("--discorduser", type=int)
# Riot
parser.add_argument("--platform", type=str)
parser.add_argument("--id", type=str)
parser.add_argument("--accountid", type=int)
parser.add_argument("--uniqueid", type=int)
parser.add_argument("--key", type=str)
# Pusher
parser.add_argument("--pusherid", type=str)
parser.add_argument("--pusherkey", type=str)
parser.add_argument("--pushersecret", type=str)
# SQL
parser.add_argument("--dbuser", type=str)
parser.add_argument("--dbpass", type=str)
parser.add_argument("--database", type=str)
parser.add_argument("--dbsocket", type=str)
args, uknown = parser.parse_known_args()

global match_history
global ids
match_history = {}
ids = []
champion = {}

# Tell the user we are starting the match history process
pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)
pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Updating match history.'})

# Star the first connection to the database.
connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)

# Set the database connection to UTF-8. For some odd reason, the database is not being connected in that charset.
connection.set_character_set('utf8')

# Initiate the first cursor.
cursor = connection.cursor()

# Get all the champion data.
cursor.execute("SELECT id, name, name_canonical FROM mm_champion WHERE lang=%s", ('en_US',))

# Loop the champion data into a new list so that a champion can be easily selected.
for id, name, name_canonical in cursor:
    champion[id] = name

# First Task: Get summoner's most recent match data (account id)
def get_matches():
    # Load match data
    res = requests.get(url="https://{}.api.riotgames.com/lol/match/v3/matchlists/by-account/{}".format(args.platform, args.accountid), headers = {"X-Riot-Token": args.key})

    # If it's legit, send it.
    if res.status_code == 200: return res.json()

    # Push any potential errors to Discord.
    if res.status_code == 400: data = {"content": "<@{}>! Bad request in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 404: data = {"content": "{} from {} has no recent matches. Probably a new or inactive account.".format(args.id, args.discorduser, args.platform.upper())}
    if res.status_code == 405: data = {"content": "<@{}>! Method not found in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code != 200: webhook(data)

# Load the matches into an array for multiprocessing.
for match in get_matches()['matches']:
    # Check if the basic match data actually exists, idk, I'm skeptical
    if not(match['gameId'] is None):
        # Check the basic match data
        cursor.execute("SELECT * FROM mm_matchlist WHERE platform_id=%s AND game_id=%s AND summoner=%s", (args.platform, match['gameId'], args.uniqueid))

        # Does this specific basic data exist in the database? Na, right?
        if cursor.rowcount == 0:
            # Append this missing ID to the list for spicer-uppers
            ids.append(match['gameId'])

            # Send to the database
            cursor.execute(
                """INSERT INTO mm_matchlist (platform_id, game_id, summoner, lane, champion, queue, role, timestamp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
                (args.platform, match['gameId'], args.uniqueid, match['lane'], match['champion'], match['queue'], match['role'], match['timestamp']))

# Let's assume there is data to work with.
connection.commit()

# Close it before we use the get_match function. New connections must be made in a function called for multiprocessing.
connection.close()

# Second Task: Get the details of each match. Lol this ain't even timeline, have you seen how large one match is? This is ridiculous.)
def get_match(id):
    # Start the connection for this process. 20 processes are going to be made, assuming the summoner has played 20 matches.
    matchdb = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)

    # Once again, set the database connection to UTF-8.
    matchdb.set_character_set('utf8')

    # Initiate the cursor.
    matchcursor = matchdb.cursor()

    # Load match by current ID
    matchcursor.execute("SELECT * FROM mm_match WHERE platform_id=%s AND game_id=%s", (args.platform, int(id)))

    # That way, you can check if the match exists before attempting to push it and insert it (errors are no fun, even when invisible).
    if matchcursor.rowcount == 0:
        res = requests.get(url="https://{}.api.riotgames.com/lol/match/v3/matches/{}".format(args.platform, id), headers = {"X-Riot-Token": args.key})
        if res.status_code == 200:
            match = res.json()
            if match:
                # Create HTML & push content to the summoner page.
                generate_match(args.platform, args.id, match, args.accountid, champion, args.pusherid, args.pusherkey, args.pushersecret)

                # Send to the database
                matchcursor.execute(
                    """INSERT INTO mm_match
                    (season_id, queue_id, game_id, participant_identities, game_version, platform_id, game_mode, map_id, game_type, teams, participants, game_duration, game_creation)
                    VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
                    (
                        match["seasonId"],
                        match["queueId"],
                        match["gameId"],
                        json.dumps(match["participantIdentities"], ensure_ascii=False),
                        match["gameVersion"],
                        args.platform,
                        match["gameMode"],
                        match["mapId"],
                        match["gameType"],
                        json.dumps(match["teams"], ensure_ascii=False),
                        json.dumps(match["participants"], ensure_ascii=False),
                        match["gameDuration"],
                        match["gameCreation"]
                    )
                )

        # Post potential errors.
        if res.status_code == 400: data = {"content": "<@{}>! Bad request in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 404: data = {"content": "<@{}>! Match {} was not found in {} for summoner {}. Wait, why are we here?".format(id, args.discorduser, args.platform.upper(), args.id)}
        if res.status_code == 405: data = {"content": "<@{}>! Method not found in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {}".format(args.discorduser, args.platform.upper())}
        if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {}.".format(args.discorduser, args.platform.upper())}
        if res.status_code != 200: webhook(data)

    # We've pushed all the matches available, now commit them to the database.
    matchdb.commit()

    # Now we're done.
    matchdb.close()

# Let's load up the pool and grab potentially 20 of the matches in para..llel....right?
pool = multiprocessing.Pool(20)
match_history = pool.imap_unordered(get_match, ids)
pool.close()
pool.join()

pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Match history updated', 'progress': 0.50})