#!/usr/bin/env python
#-*- coding: UTF-8 -*-

from pusher import Pusher
import multiprocessing
import argparse
import requests
import json

parser = argparse.ArgumentParser()
parser.add_argument("--summonername", type=str)
parser.add_argument("--summonerid", type=int)
parser.add_argument("--accountid", type=int)
parser.add_argument("--platform", type=str)
parser.add_argument("--revision", type=str)
parser.add_argument("--region", type=str)
parser.add_argument("--key", type=str)
args, uknown = parser.parse_known_args()

global summoner
global overview
summoner = {}
overview = {}

#pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)

# Initial: Get basic summoner data
def get_summoner():
    res = requests.get(url="https://{}.api.riotgames.com/lol/summoner/v3/summoners/by-name/{}".format(args.region, args.summoner), headers = {"X-Riot-Token": args.key})
    summoner['basics'] = res.json()

# First Task: Get summoner's champion mastery data (summoner id)
def get_mastery():
    res = requests.get(url="https://{}.api.riotgames.com/lol/champion-mastery/v3/champion-masteries/by-summoner/{}".format(args.region, args.id), headers = {"X-Riot-Token": args.key})
    return res.json()

#masteries = get_mastery()
#sortedmasteries = sorted(masteries, key=lambda x : x['championPoints'], reverse=True)
#print(json.dumps(sortedmasteries, ensure_ascii=False))

# Get the summoner position (summoner id)
def get_position():
    req = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.region, args.id), headers = {"X-Riot-Token": args.key})
    return req.json()

position = get_position()

# Get the positions of all summoners (league id) ##### MOVE TO ANOTHER FILE, LOAD LATER WHEN ON LEAGUE PAGE #####
def get_leagues(id):
    req = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.region, id), headers = {"X-Riot-Token": args.key})
    return req.json()

# Ready to send the basic league stats to the webpage

for standing in position:
    if 'RANKED_SOLO_5x5' in standing['queueType']:
        overview['soloduo'] = standing
    if 'RANKED_FLEX_SR' in standing['queueType']:
        overview['flexrift'] = standing
    if 'RANKED_FLEX_TT' in standing['queueType']:
        overview['flextwisted'] = standing

#print(json.dumps(overview, ensure_ascii=False))

#pusher.trigger('summoner_' + args.region + '_' + str(args.id), 'summoner_update', {'league': overview})

exit(0)