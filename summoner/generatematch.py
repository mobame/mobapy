#!/usr/bin/env python
#-*- coding: UTF-8 -*-

from dominate.tags import *
from pusher import Pusher
import timeago, datetime

matchType = {
	0    : 'Custom Game',
	70   : 'One for All',
	72   : '1v1 Snowdown',
	73   : '2v2 Snowdown',
	75   : '6v6 Hexakill',
	76   : 'URF',
	78   : 'One For All',
	83   : 'URF (Co-op)',
	98   : '6v6 Hexakill',
	100  : 'ARAM (BB)',
	310  : 'Nemesis',
    313  : 'BM Brawlers',
    317  : 'DN Dominion',
    325  : 'All Random',
    400  : 'Normal Draft',
    420  : 'Ranked Solo/Duo',
    430  : 'Normal Blind',
    440  : '5v5 Ranked Flex',
    450  : '5v5 ARAM',
    470  : '3v3 Ranked Flex',
    600  : 'Blood Hunt',
    610  : 'Cosmic Ruins',
    800  : 'Co-op (Inter.)',
    810  : 'Co-op (Intro)',
    820  : 'Co-op (Begin.)',
    830  : 'Co-op (Intro.)',
    840  : 'Co-op (Begin.)',
    850  : 'Co-op (Inter.)',
    900  : 'ARURF',
    910  : 'Ascension',
    920  : 'Poro King',
    940  : 'Nexus Siege',
    950  : 'Doom Bots (Voting)',
    960  : 'Doom Bots Standard',
    980  : 'Star Guardian (Normal)',
    990  : 'Star Guardian (Onslaught)',
    1000 : 'PROJECT: Hunters',
    1010 : 'Snow ARURF'
}

map = {
    1  : 'Summoner\'s Rift',
    2  : 'Summoner\'s Rift',
    3  : 'The Proving Grounds',
    4  : 'Twisted Treeline',
    8  : 'The Crystal Scar',
    10 : 'Twisted Treeline',
    11 : 'Summoner\'s Rift',
    12 : 'Howling Abyss',
    14 : 'Butcher\'s Bridge',
    16 : 'Cosmic Ruins',
    18 : 'Valoran City Park',
    19 : 'Substructure 43'
}

region = {
    'na1'  : 'na',
    'euw1' : 'euw',
    'eun1' : 'eune',
    'br1'  : 'br',
    'tr1'  : 'tr',
    'ru'   : 'ru',
    'la1'  : 'lan',
    'la2'  : 'las',
    'oc1'  : 'oce',
    'kr'   : 'kr',
    'jp1'  : 'jp'
}

now = datetime.datetime.now()

def generate_match(platform, id, match, accountid, champion, pusherid, pusherkey, pushersecret):
    # Setup Pusher connection
    pusher = Pusher(app_id=pusherid, key=pusherkey, secret=pushersecret, cluster='mt1', ssl=True)

	# Calculate how long ago this match was created
    timestamp = match['gameCreation'] / 1000
    value = datetime.datetime.fromtimestamp(timestamp)
    date = value.strftime('%Y-%m-%d %H:%M:%S')

    # Calculate the duration of the match in minutes & seconds
    m, s = divmod(match['gameDuration'], 60)
    time = "%2dm %2ds" % (m, s)

    for summoner in match["participantIdentities"]:
        if accountid == summoner["player"]["accountId"]:
            for participant in match["participants"]:
                if summoner['participantId'] == participant["participantId"]:
                    # Get KDA's in seperate variables
                    kills   = participant["stats"]["kills"]
                    deaths  = participant["stats"]["deaths"]
                    assists = participant["stats"]["assists"]
                    killassists = (kills + assists)

                    # Calculate Kill/Dealth/Assist ratio
                    kda = 0 if killassists == 0 else 'Perfect' if deaths == 0 else round(killassists / deaths, 2)

                    # Get total team kills to calculate current summoner's total kill participation
                    teamkills = 0
                    for killer in match["participants"]:
                        if killer["teamId"] == participant["teamId"]:
                            teamkills = teamkills + killer["stats"]["kills"]

                    # Calculate Kill Participation
                    kp  = 0 if killassists == 0 else int(round((killassists / teamkills) * 100))

                    # Style of match box depends on win/loss/remake
                    win = "remake" if match['gameDuration'] < 330 else "victory" if participant["stats"]["win"] == 1 else "defeat"

                    # Shorten gold integer
                    if participant["stats"]["goldEarned"] > 1000:
                        gold = '{}{}'.format(int(round((participant["stats"]["goldEarned"] / 1000), 0)), "K")
                    else:
                        gold = participant["stats"]["goldEarned"]

                    # Start built of match box
                    html = div(cls="match real " + win  + " mrgn-bot-ten")
                    with html:
                        attr(style="display: none;", data_summonerid=id, data_gameid=match['gameId'], data_gametime=match['gameCreation'], data_win=win)
                        # Main Container
                        with div(cls="match-container dtf rwf"):
                            # Team Section
                            with div(cls="teams dtf rwf"):
                                with div(cls="team two dtf cwf"):
                                    for teammate in match["participants"]:
                                        if 100 == teammate["teamId"]:
                                            with div(cls="player dtf stf"):
                                                with span(cls="champ"):
                                                    attr(style='background: url("https://dyno.mobame.com/img/latest/champion/' + str(teammate['championId']) + '.png") center no-repeat;background-size: 115%;')
                                                for player in match["participantIdentities"]:
                                                    if teammate["participantId"] == player['participantId']:
                                                        with a(player['player']['summonerName']):
                                                            attr(cls="name", href="https://" + region[platform] + ".mobame.com/summoner/" + player['player']['summonerName'].replace(" ", ""))
                            # KDA Section
                            with div(cls="kda dtf cwf center-flex mrgn-left-ten mrgn-right-ten"):
                                with div(cls="score csf"):
                                    with span() as s:
                                        s.add(strong(str(kills)))
                                        s.add(" \ ")
                                        s.add(strong(str(deaths)))
                                        s.add(" \ ")
                                        s.add(strong(str(assists)))
                                with div(cls="ratio csf"):
                                    with span() as s:
                                        # TODO: Calculate KDA Ratio
                                        s.add(strong(kda))
                                        s.add(" KDA")
                                with div(cls="cs csf") as d:
                                    d.add(strong(str(participant["stats"]["totalMinionsKilled"])))
                                    # TODO: Calculate CS per minute (minion + monster / total game time)
                                    d.add(" CS")
                            # Gain Section
                            with div(cls="gain dtf cwf center-flex mrgn-left-ten"):
                                with div(cls="gold csf"):
                                    with span() as s:
                                        # TODO: Calculate KDA Ratio
                                        s.add(strong(str(gold)))
                                        s.add(" Gold")
                                with div(cls="participation csf"):
                                    with span() as s:
                                        # TODO: Calculate Kill Participation
                                        s.add(strong(str(kp) + "%"))
                                        s.add(" Kill Part.")
                                with div(cls="ward csf") as d:
                                    d.add(strong(str(participant["stats"]["visionWardsBoughtInGame"])))
                                    # TODO: Calculate CS per minute (minion + monster / total game time)
                                    d.add(" Cont. Wards")
                            # Champion Section
                            with div(cls="champion dtf cwf"):
                                with div(cls="thumb csf"):
                                    attr(style='background: url("https://dyno.mobame.com/img/latest/champion/' + str(participant["championId"]) + '.png") center no-repeat;background-size: 115%;')
                                    with span(str(participant["stats"]["champLevel"])):
                                        attr(cls="level")
                                with strong(champion[participant["championId"]]):
                                    attr(cls="name csf")
                            # Summoner Section
                            with div(cls="summoners dtf csf mrgn-right-ten"):
                                with div(cls="spells"):
                                    with div(cls="summoner spell one"):
                                        img(src="https://dyno.mobame.com/img/latest/spell/" + str(participant["spell1Id"]) + ".png", alt="", title="")
                                    with div(cls="summoner spell two"):
                                        img(src="https://dyno.mobame.com/img/latest/spell/" + str(participant["spell2Id"]) + ".png", alt="", title="")
                                with div(cls="runes"):
                                    with div(cls="summoner rune one"):
                                        img(src="https://dyno.mobame.com/img/latest/perkStyle/" + str(participant["stats"]["perkPrimaryStyle"]) + ".png", alt="", title="")
                                    with div(cls="summoner rune two"):
                                        img(src="https://dyno.mobame.com/img/latest/perkStyle/" + str(participant["stats"]["perkSubStyle"]) + ".png", alt="", title="")
                            # Item Section
                            with div(cls="items dtf rwf csf"):
                                with div(cls="set dtf rwf"):
                                    with div(cls="item one"):
                                        if participant["stats"]["item0"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item0"]) + ".png", alt="", title="")
                                    with div(cls="item two"):
                                        if participant["stats"]["item1"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item1"]) + ".png", alt="", title="")
                                    with div(cls="item three"):
                                        if participant["stats"]["item2"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item2"]) + ".png", alt="", title="")
                                    with div(cls="item five"):
                                        if participant["stats"]["item3"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item3"]) + ".png", alt="", title="")
                                    with div(cls="item six"):
                                        if participant["stats"]["item4"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item4"]) + ".png", alt="", title="")
                                    with div(cls="item seven"):
                                        if participant["stats"]["item5"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item5"]) + ".png", alt="", title="")
                                with div(cls="item four csf"):
                                    if participant["stats"]["item6"] > 0 : img(src="https://dyno.mobame.com/img/latest/item/" + str(participant["stats"]["item6"]) + ".png", alt="", title="")
                            # Team Section
                            with div(cls="teams dtf rwf"):
                                with div(cls="team two dtf cwf"):
                                    for teammate in match["participants"]:
                                        if 200 == teammate["teamId"]:
                                            with div(cls="player dtf end-flex"):
                                                for player in match["participantIdentities"]:
                                                    if teammate["participantId"] == player['participantId']:
                                                        with a(player['player']['summonerName']):
                                                            attr(cls="name", href="https://" + region[platform] + ".mobame.com/summoner/" + player['player']['summonerName'].replace(" ", ""))
                                                with span(cls="champ mrgn-left-five mrgn-right-none"):
                                                    attr(style='background: url("https://dyno.mobame.com/img/latest/champion/' + str(teammate['championId']) + '.png") center no-repeat;background-size: 115%;')

                        # Ban Section
                        if match['teams'][0]['bans']:
                            with div(cls="team-bans full-flex dtf"):
                                for team in match['teams']:
                                    # Team 1
                                    if 100 == team['teamId']:
                                        with div(cls="bans left"):
                                            for ban in team['bans']:
                                                if -1 == ban['championId']:
                                                    span(cls="ban")
                                                else:
                                                    a(cls="ban", href="javascript:void();", style='background: url("https://dyno.mobame.com/img/latest/champion/' + str(ban['championId']) + '.png") center no-repeat;background-size: 115%;')
                                            span(cls='triangle')
                                    # Team 2
                                    if 200 == team['teamId']:
                                        with div(cls="bans right"):
                                            span(cls='triangle')
                                            for ban in team['bans']:
                                                if -1 == ban['championId']:
                                                    span(cls="ban")
                                                else:
                                                    a(cls="ban", href="javascript:void();", style='background: url("https://dyno.mobame.com/img/latest/champion/' + str(ban['championId']) + '.png") center no-repeat;background-size: 115%;')

                        # Extend Section (eh)
                        with div(cls="extend full-flex dtf"):
                            with div(cls="results csf") as d:
                                d.add(span(strong(win.capitalize()), cls=win), ' at ' + time + ' in ' + matchType[match['queueId']])
                            #with div(cls="replay csf"):
                                #with a(href="javascript:void(0)"):
                                    #i(" ", cls="fas fa-film")
                                    #strong("Watch Replay")
                            with div(cls="meta csf") as d:
                                d.add(timeago.format(date, now))
                                d.add(' in ')
                                d.add(map[match['mapId']])
    
                    #print(html.render(pretty=False, indent='    '))
                    pusher.trigger('summoner_' + platform + '_' + str(id), 'summoner_update', {'matchhistory': html.render(pretty=False)})