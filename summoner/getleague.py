#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import time
import argparse
import requests
import json
import math
import MySQLdb as mariadb
from multiprocessing import Pool,Manager
from dominate.tags import *
from pusher import Pusher
from postwebhooks import webhook
from urllib.parse import unquote

parser = argparse.ArgumentParser()
# General
parser.add_argument("--discorduser", type=int)
# Riot
parser.add_argument("--platform", type=str)
parser.add_argument("--id", type=str)
parser.add_argument("--accountid", type=int)
parser.add_argument("--key", type=str)
# Pusher
parser.add_argument("--pusherid", type=str)
parser.add_argument("--pusherkey", type=str)
parser.add_argument("--pushersecret", type=str)
# SQL
parser.add_argument("--dbuser", type=str)
parser.add_argument("--dbpass", type=str)
parser.add_argument("--database", type=str)
parser.add_argument("--dbsocket", type=str)
args, uknown = parser.parse_known_args()

global overview
global league_ids
#global summoner_ids

overview = {}
league_ids = []
#summoner_ids = []

connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')
cursor = connection.cursor()

# Connect to pusher, or at least authenticate
pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)

# Let the user know the league data is being grabbed.
pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Updating ranked data.'})

# Get the summoner position (summoner id)
def get_position():
    res = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.platform, args.id), headers = {"X-Riot-Token": args.key})

    if res.status_code == 400: data = {"content": "<@{}>! Bad request in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 404: data = {"content": "Summoner {} was not found in {}. Understandable if account is new or was reset.".format(args.id, args.platform.upper())}
    if res.status_code == 405: data = {"content": "<@{}>! Method not found in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 422: data = {"content": "<@{}>! {} in {} with exists, but hasn't played since match history collection began".format(args.discorduser, args.id, args.platform.upper())}
    if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code != 200: webhook(data)

    if res.status_code == 200:
        cursor.execute ("""
            UPDATE mm_summoner
            SET league_overall=%s
            WHERE id=%s AND platform_id=%s
        """, (json.dumps(res.json(), ensure_ascii=False), args.id, args.platform))
        return res.json()

position = get_position()

# Get the positions of all summoners (league id) ##### MOVE TO ANOTHER FILE, LOAD LATER WHEN ON LEAGUE PAGE #####
def get_league(id):
    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    connection.set_character_set('utf8')
    cursor = connection.cursor()

    res = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/leagues/{}".format(args.platform, id), headers = {"X-Riot-Token": args.key})

    if res.status_code == 400: data = {"content": "<@{}>! Bad League request in {}".format(args.discorduser, args.platform.upper())}
    if res.status_code == 401: data = {"content": "<@{}>! Unauthorized League request in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 403: data = {"content": "<@{}>! Forbidden League request in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 404: data = {"content": "<@{}>! League {} was not found in {}. Wait, why!? Why offer the ID then?".format(args.discorduser, id, args.platform.upper())}
    if res.status_code == 405: data = {"content": "<@{}>! Method not found in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 422: data = {"content": "<@{}>! {} in {} exists with league {}, but hasn't played since match history collection began".format(args.discorduser, args.id, args.platform.upper(), id)}
    if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {}.".format(args.discorduser, args.platform.upper())}
    if res.status_code != 200: webhook(data)

    if res.status_code == 200:
        league = res.json()
        cursor.execute("SELECT * FROM mm_league WHERE platform_id=%s AND league_id=%s", (args.platform, id))
 
        if cursor.rowcount == 1:
            cursor.execute("""UPDATE mm_league SET entries=%s WHERE platform_id=%s AND league_id=%s""",
                (
	                json.dumps(league['entries'], ensure_ascii=False),
                    args.platform,
                    id
                )
            )

        if cursor.rowcount == 0:
            cursor.execute (
                """INSERT INTO mm_league(platform_id, tier, queue, league_id, entries)VALUES (%s, %s, %s, %s, %s)""",
                (
                    args.platform,
                    league['tier'],
                    league['queue'],
                    id,
                    json.dumps(league['entries'], ensure_ascii=False)
                )
            )

        #for summoner in league['entries']:
        #    summoner_ids.append(summoner['playerOrTeamId'])

        connection.commit()
        connection.close()

        return res.json()

# Store the basic data of all the champions that've not been added yet
#def add_summoner(id):
#    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
#    connection.set_character_set('utf8')
#    cursor = connection.cursor()
#
#    cursor.execute("SELECT id FROM mm_summoner WHERE platform_id=%s AND id=%s", (args.platform, id))
#
#    # Summoner doesn't exist, we'll send data to database while putting data on html/pusher
#    if cursor.rowcount == 0:
#        sres = requests.get(url="https://{}.api.riotgames.com/lol/summoner/v3/summoners/{}".format(args.platform, id), headers = {"X-Riot-Token": args.key})
#
#        # Basic summoner data
#        summoner = sres.json()
#
#        canonical_name = summoner['name'].replace(' ', '').lower()
#
#        print(canonical_name)
#
#        #cursor.execute("""INSERT INTO mm_summoner (id, platform_id, account_id, name, name_canonical, profile_icon_id, summoner_level, revision_date, is_new, updated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)""",
#        #(id, args.platform, summoner['accountId'], summoner['name'], canonical_name, summoner['profileIconId'], summoner['summonerLevel'], summoner['revisionDate'], True, time.strftime('%Y-%m-%d %H:%M:%S')))

# Create HTML for the main rank (Solo/Duo)
def generate_main_league(data):
    wl   = math.floor(data['wins'] / (data['wins'] + data['losses']) * 100);
    html = div(id="main-rank-info", cls="container dtf center-flex rwf")
    with html:
        with div(cls="medal csf mrgn-right-five"):
            img(cls="medal-thumb", src="https://dyno.mobame.com/img/latest/tier-icons/" + data['tier'].lower() + "_" + data['rank'].lower() + ".png", alt="", title="", width="100")
        with div(cls="info csf"):
            with div(cls="item type"):
                strong('Ranked Solo/Duo')
            with div(cls="item rank") as r:
                r.add(span(data['tier'].capitalize(), ' ', span(data['rank'], ' ')))
                r.add(strong(str(data['leaguePoints']) + 'LP'))
            with div(cls="item winloss") as w:
                w.add(strong(str(data['wins']) + 'W'))
                w.add(' / ')
                w.add(strong(str(data['losses']) + 'L'))
                w.add(' (' + str(wl) + '%)')
            with div(data['leagueName']):
                attr(cls="item league")
    return html.render()

# Create HTML for any of the secondary ranks (Flex Team 5v5/SR or 3v3/TT)
def generate_other_league(data, div_id, name):
    wl   = math.floor(data['wins'] / (data['wins'] + data['losses']) * 100);
    html = div(id=div_id + "-rank-info", cls="container dtf stf rwf")
    with html:
        with div(cls="medal csf mrgn-right-five"):
            img(cls="medal-thumb", src="https://dyno.mobame.com/img/latest/tier-icons/" + data['tier'].lower() + "_" + data['rank'].lower() + ".png", alt="", title="", width="50")
        with div(cls="info csf"):
            with div(cls="item type"):
                strong(name)
            with div(cls="item rank") as r:
                r.add(span(data['tier'].capitalize(), ' ', span(data['rank'], ' ')))
                r.add(strong(str(data['leaguePoints']) + 'LP @ '))
                r.add(strong(str(data['wins']) + 'W'))
                r.add(' / ')
                r.add(strong(str(data['losses']) + 'L'))
                r.add(' (' + str(wl) + '%)')
            with div(cls="item league"):
                div(data['leagueName'])
    return html.render(pretty=False)

# Create HTML for any of the ranks that may not have a rank
def generate_unranked(maybe, name, div_id, size, flex):
    html = div(id=div_id + "-rank-info", cls="container dtf " + flex + " rwf")
    with html:
        with div(cls="medal csf mrgn-right-five"):
            img(cls="medal-thumb", src="https://dyno.mobame.com/img/latest/tier-icons/provisional.png", alt="", title="", width=size)
        with div(cls="info csf"):
            with div(cls="item type"):
                strong(name)
            with div("Currently "):
                attr(cls="item rank")
                strong('Unranked')
            div(cls="item winloss")
        if maybe == True:
            div(cls="item league")
    return html.render(pretty=True)

# Check if the summoner has a league at all, if not, return all unranked
# For extra measure, each missing rank returns individual fallbacks.
if position:
    for standing in position:
        # Ranked Solo/Duo
        if 'RANKED_SOLO_5x5' in standing['queueType']:
            league_ids.append(standing['leagueId'])
            overview['soloduo'] = generate_main_league(standing)
            cursor.execute ("""
                UPDATE mm_summoner
                SET league_solo=%s
                WHERE id=%s AND platform_id=%s
            """, (standing['leagueId'], args.id, args.platform))

        # Ranked Flex Summoner's Rift
        if 'RANKED_FLEX_SR' in standing['queueType']:
            league_ids.append(standing['leagueId'])
            overview['flexrift'] = generate_other_league(standing, "second", "Ranked Flex Summoner's Rift")
            cursor.execute ("""
                UPDATE mm_summoner
                SET league_flex_rift=%s
                WHERE id=%s AND platform_id=%s
            """, (standing['leagueId'], args.id, args.platform))

        # Ranked Flex Twisted Treeline
        if 'RANKED_FLEX_TT' in standing['queueType']:
            league_ids.append(standing['leagueId'])
            overview['flextwisted'] = generate_other_league(standing, "third", "Ranked Flex Twisted Treeline")
            cursor.execute ("""
                UPDATE mm_summoner
                SET league_flex_twisted=%s
                WHERE id=%s AND platform_id=%s
            """, (standing['leagueId'], args.id, args.platform))

if 'soloduo' not in overview : overview['soloduo'] = generate_unranked(True, "Ranked Solo/Duo", "main", "100", "center-flex")
if 'flexrift' not in overview : overview['flexrift'] = generate_unranked(False, "Ranked Flex Summoner's Rift", "second", "50", "stf")
if 'flextwisted' not in overview : overview['flextwisted'] = generate_unranked(False, "Ranked Flex Twisted Treeline", "third", "50", "stf")

# Send the basic league overview data to the frontend
pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'league': overview})

# Done with the league data. Lettem know.
pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_update', {'message': 'Ranked data updated.', 'progress': 0.25})

pool = Pool(3)
pool.map(get_league, league_ids) # Make sure to use imap_unordered
pool.close()
pool.join()

connection.commit()
connection.close()

exit(0)