#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import time
import math
import requests
import json
import MySQLdb as mariadb
import multiprocessing
from postwebhooks import webhook
from urllib.parse import unquote

global codes
global newattempt
global summoner_id
global summoner_unique
codes = []

def link_summoner(code):
    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    connection.set_character_set('utf8')
    cursor = connection.cursor()

    cursor.execute("SELECT user, summoner, main, platform_id, attempts FROM mm_link WHERE code=%s",(code,))

    for user, summoner, main, platform_id, attempts, in cursor:

        cursor.execute("UPDATE mm_link SET last_attempted=%s, attempts=%s WHERE code=%s",(time.strftime('%Y-%m-%d %H:%M:%S'), (attempts + 1), code,))

        connection.commit()

        cursor.execute("SELECT id FROM mm_summoner WHERE unique_id=%s",(summoner,))

        for (id,) in cursor:
            summoner_id = id

            res = requests.get(url="https://{}.api.riotgames.com/lol/platform/v3/third-party-code/by-summoner/{}".format(platform_id, summoner_id), headers = {"X-Riot-Token": "RGAPI-d26ddd7d-cb6e-cd7e-30ad-f2a4c37161f3"})

            # If it's legit, send it.
            if res.status_code == 200:
                if code == res.json():
                    # If there's already a summoner that's a main, change it to a smurf
                    if main == True:
                        cursor.execute("UPDATE mm_summoner SET main=%s WHERE owner=%s AND main=%s",(False, user, main))

                    # Update the chosen summoner
                    cursor.execute("UPDATE mm_summoner SET owner=%s, main=%s WHERE unique_id=%s",(user, main, summoner))

                    # Remove the link process as the verification is done
                    cursor.execute("DELETE FROM mm_link WHERE code=%s", (code,))

                    # Esketit
                    connection.commit()

                    print('Summoner is linked!')
                else:
                    print('The wrong code was entered.')

            # Push any potential errors to Discord.
            if res.status_code == 400: data = {"content": "<@{}>! Bad request in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 404: data = {"content": "{} from {} has no verification key set. Probably a new or inactive account.".format(summoner_id, args.discorduser, platform_id.upper())}
            if res.status_code == 405: data = {"content": "<@{}>! Method not found in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {}.".format(args.discorduser, platform_id.upper())}
            if res.status_code != 200: webhook(data)

    connection.close()

connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
connection.set_character_set('utf8')
cursor = connection.cursor()

cursor.execute("SELECT code FROM mm_link WHERE ready=%s LIMIT 10", (1,))

if cursor.rowcount > 0:
    for (code,) in cursor:
        codes.append(code)

    pool = multiprocessing.Pool(cursor.rowcount)
    match_history = pool.map(link_summoner, codes)
    pool.close()
    pool.join()
else:
    print('No links to process.')

connection.close()
exit(0)