#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import argparse
import requests
import json
import math
import multiprocessing
import MySQLdb as mariadb
from pusher import Pusher
from dominate.tags import *
from postwebhooks import webhook
from urllib.parse import unquote

parser = argparse.ArgumentParser()
# General
parser.add_argument("--discorduser", type=int)
# Riot
parser.add_argument("--platform", type=str)
parser.add_argument("--id", type=str)
parser.add_argument("--key", type=str)
# Pusher
parser.add_argument("--pusherid", type=str)
parser.add_argument("--pusherkey", type=str)
parser.add_argument("--pushersecret", type=str)
# SQL
parser.add_argument("--dbuser", type=str)
parser.add_argument("--dbpass", type=str)
parser.add_argument("--database", type=str)
parser.add_argument("--dbsocket", type=str)
args, uknown = parser.parse_known_args()

global game
global team_one_players
global team_two_players
game = {}
team_one_players = []
team_two_players = []
player_league = {}

matchType = {
	0    : 'Custom Game',
	70   : 'One for All',
	72   : '1v1 Snowdown',
	73   : '2v2 Snowdown',
	75   : '6v6 Hexakill',
	76   : 'URF',
	78   : 'One For All',
	83   : 'URF (Co-op)',
	98   : '6v6 Hexakill',
	100  : 'ARAM (BB)',
	310  : 'Nemesis',
    313  : 'BM Brawlers',
    317  : 'DN Dominion',
    325  : 'All Random',
    400  : 'Normal Draft',
    420  : 'Ranked Solo/Duo',
    430  : 'Normal Blind',
    440  : '5v5 Ranked Flex',
    450  : '5v5 ARAM',
    470  : '3v3 Ranked Flex',
    600  : 'Blood Hunt',
    610  : 'Cosmic Ruins',
    800  : 'Co-op (Inter.)',
    810  : 'Co-op (Intro)',
    820  : 'Co-op (Begin.)',
    830  : 'Co-op (Intro.)',
    840  : 'Co-op (Begin.)',
    850  : 'Co-op (Inter.)',
    900  : 'ARURF',
    910  : 'Ascension',
    920  : 'Poro King',
    940  : 'Nexus Siege',
    950  : 'Doom Bots (Voting)',
    960  : 'Doom Bots Standard',
    980  : 'Star Guardian (Normal)',
    990  : 'Star Guardian (Onslaught)',
    1000 : 'PROJECT: Hunters',
    1010 : 'Snow ARURF'
}

map = {
    1  : 'Summoner\'s Rift',
    2  : 'Summoner\'s Rift',
    3  : 'The Proving Grounds',
    4  : 'Twisted Treeline',
    8  : 'The Crystal Scar',
    10 : 'Twisted Treeline',
    11 : 'Summoner\'s Rift',
    12 : 'Howling Abyss',
    14 : 'Butcher\'s Bridge',
    16 : 'Cosmic Ruins',
    18 : 'Valoran City Park',
    19 : 'Substructure 43'
}

region = {
    'na1'  : 'na',
    'euw1' : 'euw',
    'eun1' : 'eune',
    'br1'  : 'br',
    'tr1'  : 'tr',
    'ru'   : 'ru',
    'la1'  : 'lan',
    'la2'  : 'las',
    'oc1'  : 'oce',
    'kr'   : 'kr',
    'jp1'  : 'jp'
}

role_order = {
	'TOP': 1,
	'JUNGLE': 2,
	'MID': 3,
	'ADC': 4,
	'SUPPORT': 5
}

pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)

# Initial: Get spectator game data
def get_gameinfo():
    res = requests.get(url="https://{}.api.riotgames.com/lol/spectator/v3/active-games/by-summoner/{}".format(args.platform, args.id), headers = {"X-Riot-Token": args.key})

    if res.status_code == 200: game['game'] = res.json()
    if res.status_code == 400: data = {"content": "<@145621250138570752>! Bad request in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 401: data = {"content": "<@145621250138570752>! Unauthorized in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 403: data = {"content": "<@145621250138570752>! Forbidden in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 404: return False
    if res.status_code == 405: data = {"content": "<@145621250138570752>! Method not found in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 415: data = {"content": "<@145621250138570752>! Unsupported media type in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 429: data = {"content": "<@145621250138570752>! Rate limit exceeded in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 500: data = {"content": "<@145621250138570752>! Internal server error in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 502: data = {"content": "<@145621250138570752>! Bad gateway in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 503: data = {"content": "<@145621250138570752>! Service Unavailable in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code == 504: data = {"content": "<@145621250138570752>! Gateway timeout in {} while looking up game for summoner {}".format(args.platform.upper(), args.id)}
    if res.status_code != 200: webhook(data)    

#game['game'] = {"gameId": 2736075035, "mapId": 11, "gameMode": "CLASSIC", "gameType": "MATCHED_GAME", "gameQueueConfigId": 420, "participants": [{"teamId": 100, "spell1Id": 4, "spell2Id": 12, "championId": 6, "profileIconId": 3368, "summonerName": "BrohammerJoe", "bot": 0, "summonerId": 35102038, "gameCustomizationObjects": [], "perks": {"perkIds": [8124, 8143, 8136, 8105, 8243, 8234], "perkStyle": 8100, "perkSubStyle": 8200}}, {"teamId": 100, "spell1Id": 4, "spell2Id": 14, "championId": 103, "profileIconId": 1109, "summonerName": "dweeb xD", "bot": 0, "summonerId": 19792444, "gameCustomizationObjects": [], "perks": {"perkIds": [8112, 8143, 8138, 8135, 8210, 8237], "perkStyle": 8100, "perkSubStyle": 8200}}, {"teamId": 100, "spell1Id": 4, "spell2Id": 11, "championId": 154, "profileIconId": 3366, "summonerName": "Wasuremono", "bot": 0, "summonerId": 56460978, "gameCustomizationObjects": [], "perks": {"perkIds": [8439, 8473, 8429, 8242, 8136, 8134], "perkStyle": 8400, "perkSubStyle": 8100}}, {"teamId": 100, "spell1Id": 7, "spell2Id": 4, "championId": 18, "profileIconId": 753, "summonerName": "That1guysBro", "bot": 0, "summonerId": 25785878, "gameCustomizationObjects": [], "perks": {"perkIds": [8021, 9101, 9103, 8014, 8233, 8236], "perkStyle": 8000, "perkSubStyle": 8200}}, {"teamId": 100, "spell1Id": 3, "spell2Id": 4, "championId": 267, "profileIconId": 577, "summonerName": "THzWeeds", "bot": 0, "summonerId": 27753296, "gameCustomizationObjects": [], "perks": {"perkIds": [8214, 8226, 8234, 8236, 8463, 8453], "perkStyle": 8200, "perkSubStyle": 8400}}, {"teamId": 200, "spell1Id": 4, "spell2Id": 7, "championId": 81, "profileIconId": 1113, "summonerName": "ROFLMFAO LOL", "bot": 0, "summonerId": 88091841, "gameCustomizationObjects": [], "perks": {"perkIds": [8359, 8304, 8321, 8347, 8226, 8210], "perkStyle": 8300, "perkSubStyle": 8200}}, {"teamId": 200, "spell1Id": 4, "spell2Id": 14, "championId": 44, "profileIconId": 3232, "summonerName": "Gankr", "bot": 0, "summonerId": 51146953, "gameCustomizationObjects": [], "perks": {"perkIds": [8465, 8473, 8429, 8453, 8304, 8347], "perkStyle": 8400, "perkSubStyle": 8300}}, {"teamId": 200, "spell1Id": 4, "spell2Id": 11, "championId": 19, "profileIconId": 1227, "summonerName": "Technique", "bot": 0, "summonerId": 32503409, "gameCustomizationObjects": [], "perks": {"perkIds": [8005, 9111, 9104, 8014, 8243, 8234], "perkStyle": 8000, "perkSubStyle": 8200}}, {"teamId": 200, "spell1Id": 4, "spell2Id": 12, "championId": 54, "profileIconId": 3199, "summonerName": "alatank", "bot": 0, "summonerId": 23698962, "gameCustomizationObjects": [], "perks": {"perkIds": [8229, 8226, 8234, 8237, 8473, 8444], "perkStyle": 8200, "perkSubStyle": 8400}}, {"teamId": 200, "spell1Id": 4, "spell2Id": 12, "championId": 4, "profileIconId": 3369, "summonerName": "A NEW MONSTER", "bot": 0, "summonerId": 43627003, "gameCustomizationObjects": [], "perks": {"perkIds": [8229, 8226, 8234, 8237, 8347, 8313], "perkStyle": 8200, "perkSubStyle": 8300}}], "observers": {"encryptionKey": "xNXryKC3uFTSSZbVNH/9Itz9840UPXCp"}, "platformId": "NA1", "bannedChampions": [{"championId": 121, "teamId": 100, "pickTurn": 1}, {"championId": 72, "teamId": 100, "pickTurn": 2}, {"championId": 28, "teamId": 100, "pickTurn": 3}, {"championId": 51, "teamId": 100, "pickTurn": 4}, {"championId": 145, "teamId": 100, "pickTurn": 5}, {"championId": 8, "teamId": 200, "pickTurn": 6}, {"championId": 40, "teamId": 200, "pickTurn": 7}, {"championId": 164, "teamId": 200, "pickTurn": 8}, {"championId": 117, "teamId": 200, "pickTurn": 9}, {"championId": 121, "teamId": 200, "pickTurn": 10}], "gameStartTime": 1520572864837, "gameLength": 1644}

# Metadata sections

def generate_bans(bans):
    html = div(id="game-team-bans", cls="dtf rwf")
    with html:
        with div(cls="team one dtf half-flex stf"):
            div(i(cls="fas fa-gavel blue"), ' ', em(strong("BANS")), span(cls="triangle dark-bg"), cls="title csf")
            with div(cls="ban-set one dtf rwf stf csf"):
                for ban in bans:
                    if ban['teamId'] == 100:
                        div(i(data_champion=str(ban["championId"])), cls="ban")
        with div(cls="team two dtf half-flex end-flex"):
            with div(cls="ban-set two dtf rwf stf csf"):
                for ban in bans:
                    if ban['teamId'] == 200:
                        div(i(data_champion=str(ban["championId"])), cls="ban")
            div(span(cls="triangle dark-bg"), strong("BANS"), ' ', i(data_fa_transform="rotate--90", cls="fas fa-gavel red"), cls="title csf")
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'team_bans': html.render(pretty=False)})

def generate_team_one(player):
    global player_league

    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    connection.set_character_set('utf8')
    cursor = connection.cursor()

    cursor.execute("SELECT id, name, summoner_level, league_overall FROM mm_summoner WHERE platform_id=%s AND id=%s", (args.platform, player['summonerId']))

    # Summoner doesn't exist, we'll send data to database while putting data on html/pusher
    if cursor.rowcount == 0:
        sres = requests.get(url="https://{}.api.riotgames.com/lol/summoner/v3/summoners/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})

        # Basic summoner data
        summoner = sres.json()
        level = summoner['summonerLevel']

        # Summoner league data
        lres = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})
        league = lres.json()

        #cursor.execute("""INSERT INTO mm_matchlist (platform_id, game_id, summoner, lane, champion, queue, role, timestamp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
        #(args.platform, match['gameId'], args.uniqueid, match['lane'], match['champion'], match['queue'], match['role'], match['timestamp']))

    # Champion exists, so we'll use what we have
    if cursor.rowcount == 1:
        for id, name, summoner_level, league_overall in cursor:
            level = summoner_level

            if league_overall:
                league = json.loads(league_overall)
            else:
                lres = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})
                league = lres.json()

    if league is not None:
        for standing in league:
            #Check if 5v5 Ranked Flex SR
            if '440' in str(game['game']['gameQueueConfigId']):
                if 'RANKED_FLEX_SR' in standing['queueType']:
                    player_league = standing
            #Check if 3v3 Ranked Flex TT
            elif '440' in str(game['game']['gameQueueConfigId']):
                if 'RANKED_FLEX_TT' in standing['queueType']:
                    player_league = standing
            #Check if Ranked Solo/Duo
            else:
                if 'RANKED_SOLO_5x5' in standing['queueType']:
                    player_league = standing

    html = div(cls="player dtf rwf posr")
    with html:
        with div(cls="left dtf rwf"):
            with div(cls="dtf champion mrgn-right-ten"):
                div(cls="dtf icon mrgn-right-five", style="background: url(https://dyno.mobame.com/img/latest/champion/" + str(player['championId']) + ".png) center no-repeat;")
                with div(cls="summoners"):
                    div(i(data_spell=str(player['spell1Id'])), cls="summoner one")
                    div(i(data_spell=str(player['spell2Id'])), cls="summoner two")
            with div(cls="basics dtf cwf center-flex csf"):
                div(strong((player['summonerName'][:9] + '..') if len(player['summonerName']) > 9 else player['summonerName']), cls="name top")
                div("Level " + str(level), cls="level")
        with div(cls="statistics dtf rwf"):
            with div(cls="ranked-container center dtf rwf stf"):
                if 'tier' in player_league:
                    wl = math.floor(player_league['wins'] / (player_league['wins'] + player_league['losses']) * 100);
                    with div(cls="ranked dtf rwf csf"):
                        div(i(data_rank=player_league['tier'].lower() + "_" + player_league['rank'].lower()), cls="medal mrgn-right-five")
                    with div(cls="stats csf"):
                        div(span(player_league['tier'].capitalize(), " ", player_league['rank'], " ", style="color: #F02;"), strong(str(player_league['leaguePoints']) + "LP"), cls="rank top")
                        div(span(str(player_league['wins']) + "W"), " / ", span(str(player_league['losses']) + "L"), "  (" + str(wl) + "%)", cls="win-loss")
                else:
                    with div(cls="ranked dtf rwf csf"):
                        div(i(data_rank="provisional"), cls="medal mrgn-right-five")
                    with div(cls="stats csf"):
                        div("Currently ", strong("Unranked"))
            with div(cls="center csf"):
                div(strong("0W"), " / ", strong("0L"), " ", "(0%)", cls="top")
                div(strong("0"), " / ", strong("0"), " / ", strong("0"), " ", "(0)", cls="kda")
        with div(cls="runes dtf rwf csf"):
            with div(cls="main inspiration dtf rwf"):
                i(data_rune=str(player['perks']['perkIds'][0]))
                i(data_rune=str(player['perks']['perkIds'][1]))
                i(data_rune=str(player['perks']['perkIds'][2]))
                i(data_rune=str(player['perks']['perkIds'][3]))
            with div(cls="sub sorcery dtf rwf"):
                i(data_rune=str(player['perks']['perkIds'][4]))
                i(data_rune=str(player['perks']['perkIds'][5]))
        div(i(cls="fas fa-crosshairs"),' Runes', cls="rune-toggle right button csf")

    pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'team_one': html.render(pretty=False)})

def generate_team_two(player):
    global player_league

    connection = mariadb.connect(user=args.dbuser, password=unquote(args.dbpass), database=args.database, unix_socket=args.dbsocket)
    connection.set_character_set('utf8')
    cursor = connection.cursor()

    cursor.execute("SELECT id, name, summoner_level, league_overall FROM mm_summoner WHERE platform_id=%s AND id=%s", (args.platform, player['summonerId']))

    # Summoner doesn't exist, we'll send data to database while putting data on html/pusher
    if cursor.rowcount == 0:
        sres = requests.get(url="https://{}.api.riotgames.com/lol/summoner/v3/summoners/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})

        # Basic summoner data
        summoner = sres.json()
        level = summoner['summonerLevel']

        # Summoner league data
        lres = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})
        league = lres.json()

        #cursor.execute("""INSERT INTO mm_matchlist (platform_id, game_id, summoner, lane, champion, queue, role, timestamp) VALUES (%s, %s, %s, %s, %s, %s, %s, %s)""",
        #(args.platform, match['gameId'], args.uniqueid, match['lane'], match['champion'], match['queue'], match['role'], match['timestamp']))

    # Summoner exists, so we'll use what we have
    if cursor.rowcount == 1:
        for id, name, summoner_level, league_overall in cursor:
            level = summoner_level

            if league_overall:
                league = json.loads(league_overall)
            else:
                lres = requests.get(url="https://{}.api.riotgames.com/lol/league/v3/positions/by-summoner/{}".format(args.platform, player['summonerId']), headers = {"X-Riot-Token": args.key})
                league = lres.json()

    if league is not None:
        for standing in league:
            #Check if 5v5 Ranked Flex SR
            if '440' in str(game['game']['gameQueueConfigId']):
                if 'RANKED_FLEX_SR' in standing['queueType']:
                    player_league = standing
            #Check if 3v3 Ranked Flex TT
            elif '440' in str(game['game']['gameQueueConfigId']):
                if 'RANKED_FLEX_TT' in standing['queueType']:
                    player_league = standing
            #Check if Ranked Solo/Duo
            else:
                if 'RANKED_SOLO_5x5' in standing['queueType']:
                    player_league = standing

    html = div(cls="player dtf rwf posr")
    with html:
        div(i(cls="fas fa-crosshairs"),' Runes', cls="rune-toggle right button csf")
        with div(cls="statistics dtf rwf"):
            with div(cls="center csf rght-txt"):
                div(strong("0W"), " / ", strong("0L"), " ", "(0%)", cls="top")
                div(strong("0"), " / ", strong("0"), " / ", strong("0"), " ", "(0)", cls="kda")
            with div(cls="ranked-container center dtf rwf end-flex"):
                if 'tier' in player_league:
                    wl = math.floor(player_league['wins'] / (player_league['wins'] + player_league['losses']) * 100);
                    with div(cls="stats csf rght-txt"):
                        div(span(player_league['tier'].capitalize(), " ", player_league['rank'], " ", style="color: #F02;"), strong(str(player_league['leaguePoints']) + "LP"), cls="rank top")
                        div(span(str(player_league['wins']) + "W"), " / ", span(str(player_league['losses']) + "L"), "  (" + str(wl) + "%)", cls="win-loss")
                    with div(cls="ranked dtf rwf csf"):
                        div(i(data_rank=player_league['tier'].lower() + "_" + player_league['rank'].lower()), cls="medal mrgn-left-five")
                else:
                    with div(cls="ranked dtf rwf csf"):
                        div(i(data_rank="provisional"), cls="medal mrgn-right-five")
                    with div(cls="stats csf"):
                        div("Currently ", strong("Unranked"))
        with div(cls="runes dtf rwf csf"):
            with div(cls="main inspiration dtf rwf"):
                i(data_rune=str(player['perks']['perkIds'][0]))
                i(data_rune=str(player['perks']['perkIds'][1]))
                i(data_rune=str(player['perks']['perkIds'][2]))
                i(data_rune=str(player['perks']['perkIds'][3]))
            with div(cls="sub sorcery dtf rwf"):
                i(data_rune=str(player['perks']['perkIds'][4]))
                i(data_rune=str(player['perks']['perkIds'][5]))
        with div(cls="right dtf rwf"):
            with div(cls="basics dtf cwf center-flex csf rght-txt"):
                div(strong((player['summonerName'][:9] + '..') if len(player['summonerName']) > 9 else player['summonerName']), cls="name top")
                div("Level " + str(level), cls="level")
            with div(cls="dtf champion mrgn-left-ten"):
                with div(cls="summoners"):
                    div(i(data_spell=str(player['spell1Id'])), cls="summoner one")
                    div(i(data_spell=str(player['spell2Id'])), cls="summoner two")
                div(cls="dtf icon mrgn-left-five", style="background: url(https://dyno.mobame.com/img/latest/champion/" + str(player['championId']) + ".png) center no-repeat;")

    pusher = Pusher(app_id=args.pusherid, key=args.pusherkey, secret=args.pushersecret, cluster='mt1', ssl=True)
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'team_two': html.render(pretty=False)})

# Initiate start of getting summoner
get_gameinfo()

# Make it easier to reduce errors.
if 'game' in game:
    if game['game']['gameLength'] > 0:
        # Calculate the duration of the match in minutes & seconds
        m, s = divmod(game['game']['gameLength'], 60)
        time = "%2dm %2ds" % (m, s)
    else:
        time = '0m 0s'

    # Send metadata first
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'type': matchType[game['game']['gameQueueConfigId']], 'time': time, 'map': map[game['game']['mapId']]})

    # Loop each team's players into that team's array since they're scrambled
    for player in game['game']["participants"]:
        # Blue team
        if player['teamId'] == 100:
            team_one_players.append(player)
        # Red team
        if player['teamId'] == 200:
            team_two_players.append(player)

    # Both teams are processed individually
    pool = multiprocessing.Pool(10)
    pool.imap_unordered(generate_team_one, team_one_players) # Make sure to only use imap_unordered in production
    pool.imap_unordered(generate_team_two, team_two_players)
    pool.close()
    pool.join()

    # Enable ban section
    if game['game']['bannedChampions'] is not None:
        generate_bans(game['game']['bannedChampions'])

    # We good? Cool.
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'done': True})
else:
    pusher.trigger('summoner_' + args.platform + '_' + str(args.id), 'summoner_game', {'failed': True})
exit(0)