#!/usr/bin/env python
#-*- coding: UTF-8 -*-

import argparse
import requests
import json
from postwebhooks import webhook

parser = argparse.ArgumentParser()
# General
parser.add_argument("--discorduser", type=int)
# Riot
parser.add_argument("--platform", type=str)
parser.add_argument("--summoner", type=str)
parser.add_argument("--key", type=str)
args, uknown = parser.parse_known_args()

global summoner
summoner = {}

# Initial: Get basic summoner data
def get_summoner():
    res = requests.get(url="https://{}.api.riotgames.com/lol/summoner/v3/summoners/by-name/{}".format(args.platform, args.summoner), headers = {"X-Riot-Token": args.key})

    if res.status_code == 200: summoner['basics'] = res.json()
    if res.status_code == 400: data = {"content": "<@{}>! Bad request in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 401: data = {"content": "<@{}>! Unauthorized in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 403: data = {"content": "<@{}>! Forbidden in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 404: data = {"content": "Summoner {} was not found in {}".format(args.summoner, args.platform.upper())}
    if res.status_code == 405: data = {"content": "<@{}>! Method not found in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 415: data = {"content": "<@{}>! Unsupported media type in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 429: data = {"content": "<@{}>! Rate limit exceeded in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 500: data = {"content": "<@{}>! Internal server error in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 502: data = {"content": "<@{}>! Bad gateway in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 503: data = {"content": "<@{}>! Service Unavailable in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code == 504: data = {"content": "<@{}>! Gateway timeout in {} while looking up summoner {}".format(args.discorduser, args.platform.upper(), args.summoner)}
    if res.status_code != 200: webhook(data)    

# Initiate start of getting summoner
get_summoner()

# Esketit
print(json.dumps(summoner, ensure_ascii=False))

exit(0)